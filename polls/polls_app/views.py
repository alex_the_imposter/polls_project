from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone

from .models import Question as qs, Choice as ch


class IndexView(generic.ListView):
    template_name = 'polls_app/home.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """
        Return the last five published questions(
        not including those to be published in the future).
        """

        return qs.objects.filter(
            pub_date__lte=timezone.now()
        )


class DetailView(generic.DetailView):
    model = qs
    template_name = 'polls_app/detail.html'

    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        return qs.objects.filter(pub_date__lte=timezone.now())


class ResultsView(generic.DetailView):
    model = qs
    template_name = 'polls_app/result.html'


def vote(request, question_id):
    question = get_object_or_404(qs, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, ch.DoesNotExist):
        return render(request, 'polls_app/detail.html', {
            'question': question,
            'err_msg': 'You didn\'t select a choice'
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()

        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('polls_app:results', args=(question.id,)))

