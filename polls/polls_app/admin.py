from django.contrib import admin
from .models import Choice, Question


class ChoiceCustomize(admin.TabularInline):
    model = Choice
    extra = 3


class QuestionCustomize(admin.ModelAdmin):
    fieldsets = [
        ('Question info', {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date']}),
    ]
    inlines = [ChoiceCustomize]
    list_display = ('question_text', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['question_text']
    list_per_page = 3


admin.site.register(Question, QuestionCustomize)
# admin.site.register(Choice)
