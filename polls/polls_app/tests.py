from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from .models import Question

import datetime


def create_question(question_text, days):
    """
    Create a question with the given 'question_text' and published given number of 'days'
    offset to now (negative for questions in the past, positive for questions
    that have yet to be published).
    """

    time = timezone.now() + datetime.timedelta(days=days)
    return Question.objects.create(
        question_text=question_text,
        pub_date=time
    )


class QuestionModelTests(TestCase):

    def test_was_published_recently_with_future_question(self):
        """
        was_published_recently() returns False
        for the question whose pub_date in the future
        """

        time = timezone.now() + datetime.timedelta(days=20)
        question_with_pub_date_in_future = Question(pub_date=time)
        self.assertIs(question_with_pub_date_in_future.was_published_recently(), False)

    def test_was_published_recently_with_old_question(self):
        """
        was_published_recently() returns False
        for the question whose pub_date is older than 1 day.
        """

        time = timezone.now() - datetime.timedelta(days=1, seconds=1)
        question_with_pub_date_in_past = Question(pub_date=time)
        self.assertIs(question_with_pub_date_in_past.was_published_recently(), False)

    def test_was_published_recently_with_recent_question(self):
        """
        was_published_recently() returns True
        for questions whose pub_date is within the last day.
        """

        time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=57)
        question_with_recent_pub_date = Question(pub_date=time)
        self.assertIs(question_with_recent_pub_date.was_published_recently(), True)


class QuestionIndexViewTests(TestCase):
    def test_no_questions(self):
        """
        If no question exist, an appropriate message is displayed.
        """

        response = self.client.get(reverse('polls_app:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'No polls are available.')
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_past_question(self):
        """
        Question with a pub_date in the past are displayed on the index page.
        """

        create_question(question_text='past question', days=-30)
        response = self.client.get(reverse('polls_app:index'))
        self.assertQuerysetEqual(
            response.context['latest_question_list'], ['<Question: past question>']
        )

    def test_future_question(self):
        """
        Question with a pub_date in the future aren't displayed on the index page.
        """

        create_question(question_text='future question', days=23)
        response = self.client.get(reverse('polls_app:index'))
        self.assertContains(response, 'No polls are available.')
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_future_and_past_questions(self):
        """
        Even if past and future questions exist, only past questions are displayed.
        """

        create_question(question_text='past question', days=-30)
        create_question(question_text='future question', days=34)
        response = self.client.get(reverse('polls_app:index'))
        self.assertQuerysetEqual(
            response.context['latest_question_list'], ['<Question: past question>']
        )

    def test_two_past_questions(self):
        """
        The questions index page may display multiple questions.
        """

        create_question(question_text="Past question 1.", days=-30)
        create_question(question_text="Past question 2.", days=-5)
        response = self.client.get(reverse('polls_app:index'))
        print(response.context['latest_question_list'])
        self.assertQuerysetEqual(
            response.context['latest_question_list'],
            ['<Question: Past question 1.>', '<Question: Past question 2.>'], ordered=False
        )


class QuestionDetailViewTests(TestCase):
    def test_future_question(self):
        """
        The detail view of a question with a pub_date in the future returns a 404 not found.
        """

        future_question = create_question(question_text='future question', days=6)
        url = reverse('polls_app:detail', args=(future_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        """
        The detail view of a question with a pub_date in the past
        displays the question's text.
        """

        past_question = create_question(question_text='past question', days=-4)
        url = reverse('polls_app:detail', args=(past_question.id,))
        response = self.client.get(url)
        self.assertContains(response, past_question.question_text)
